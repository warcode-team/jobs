package com.warcode.jobs;

import com.warcode.jobs.dto.CreateJobRequest;
import com.warcode.jobs.dto.UpdateJobRequest;
import com.warcode.jobs.enitity.Job;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
class JobsService {

    private final JobsRepository repository;

    List<Job> listJobs() {
        return repository.findAll();
    }

    Job getJob(long jobId) {
        return repository.findById(jobId).orElseThrow(JobNotFoundException::new);
    }

    Job createJob(CreateJobRequest createJobRequest) {
        return repository.save(createJobRequest.toJob());
    }

    void updateJob(long jobId, UpdateJobRequest request) {
        Job revisedJob = getJob(jobId);

        revisedJob.setTitle(request.title());
        revisedJob.setDescription(request.description());
        revisedJob.setStatus(request.status());
        revisedJob.setApplyUntil(request.applyUntil());
        revisedJob.setValidUntil(request.validUntil());

        repository.save(revisedJob);
    }

    void deleteJob(long jobId) {
        repository.deleteById(jobId);
    }

    List<Job> search(Integer employerId) {
        if (employerId != null) {
            return repository.getAllByEmployerId(employerId);
        }

        return repository.findAll();
    }
}
