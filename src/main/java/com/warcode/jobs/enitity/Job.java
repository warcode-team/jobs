package com.warcode.jobs.enitity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.OffsetDateTime;

@Entity
@Table(name = "jobs")
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Job {

    public enum JobStatus {
        DRAFT, READY, POSTED
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    private Integer employerId;

    @NotBlank
    private String title;

    @NotBlank
    private String description;

    @Builder.Default
    private JobStatus status = JobStatus.DRAFT;

    private OffsetDateTime validUntil;
    private OffsetDateTime applyUntil;

    @CreationTimestamp
    private OffsetDateTime createdOn;

    @UpdateTimestamp
    private OffsetDateTime updatedOn;
}
