package com.warcode.jobs.dto;

import com.warcode.jobs.enitity.Job.JobStatus;

import java.time.OffsetDateTime;

public record UpdateJobRequest(
        String title,
        String description,
        OffsetDateTime validUntil,
        OffsetDateTime applyUntil,
        JobStatus status) {
}