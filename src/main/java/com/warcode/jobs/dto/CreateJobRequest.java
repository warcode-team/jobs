package com.warcode.jobs.dto;

import com.warcode.jobs.enitity.Job;
import com.warcode.jobs.enitity.Job.JobStatus;
import lombok.AllArgsConstructor;
import lombok.Builder;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.OffsetDateTime;

@Builder
@AllArgsConstructor
public record CreateJobRequest(
        @NotNull int employerId,
        @NotBlank String title,
        @NotBlank String description,
        @NotNull JobStatus status,
        OffsetDateTime validUntil,
        OffsetDateTime applyUntil
) {

    public Job toJob() {
        return Job.builder()
                  .employerId(employerId)
                  .title(title)
                  .description(description)
                  .status(status)
                  .validUntil(validUntil)
                  .applyUntil(applyUntil)
                  .build();
    }
}
