package com.warcode.jobs;

import com.warcode.jobs.enitity.Job;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface JobsRepository extends JpaRepository<Job, Long> {
    List<Job> getAllByEmployerId(int employerId);
}
