package com.warcode.jobs;

import com.warcode.jobs.dto.CreateJobRequest;
import com.warcode.jobs.dto.UpdateJobRequest;
import com.warcode.jobs.enitity.Job;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/jobs")
@RequiredArgsConstructor
class JobsController {

    private final JobsService jobsService;

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<Job> listJobs() {
        return jobsService.listJobs();
    }

    @GetMapping("/search")
    @ResponseStatus(HttpStatus.OK)
    public List<Job> findJobs(@RequestParam(required = false) Integer employerId) {
        return jobsService.search(employerId);
    }

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Job getJob(@PathVariable long id) {
        return jobsService.getJob(id);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Job createJob(@RequestBody @Valid CreateJobRequest createJobRequest) {
        return jobsService.createJob(createJobRequest);
    }

    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void updateJob(@PathVariable long id, @RequestBody UpdateJobRequest request) {
        jobsService.updateJob(id, request);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteJob(@PathVariable long id) {
        jobsService.deleteJob(id);
    }
}
