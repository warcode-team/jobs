package com.warcode.jobs;

class JobNotFoundException extends RuntimeException {
    private static final String DEFAULT_MESSAGE = "Job not found.";

    JobNotFoundException() {
        super(DEFAULT_MESSAGE);
    }
}
