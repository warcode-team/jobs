package com.warcode.jobs;

import com.warcode.jobs.dto.CreateJobRequest;
import com.warcode.jobs.dto.UpdateJobRequest;
import com.warcode.jobs.enitity.Job;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class JobsServiceTest {

    @Mock
    private JobsRepository repository;

    @InjectMocks
    private JobsService jobsService;

    @Test
    void listsJobs() {
        Job job1 = Job.builder()
                      .employerId(1)
                      .title("title-1")
                      .description("description-1")
                      .applyUntil(OffsetDateTime.now(ZoneOffset.UTC).plusDays(1))
                      .validUntil(OffsetDateTime.now(ZoneOffset.UTC).plusDays(2))
                      .build();

        Job job2 = Job.builder()
                      .employerId(2)
                      .title("title-2")
                      .description("description-2")
                      .applyUntil(OffsetDateTime.now(ZoneOffset.UTC).plusDays(1))
                      .validUntil(OffsetDateTime.now(ZoneOffset.UTC).plusDays(2))
                      .build();

        List<Job> jobsList = List.of(job1, job2);

        when(repository.findAll()).thenReturn(jobsList);

        Iterable<Job> jobs = jobsService.listJobs();

        assertThat(jobs).containsExactlyInAnyOrderElementsOf(jobsList);
    }

    @Test
    void getsJobById() {
        Job job = Job.builder()
                     .employerId(2)
                     .title("title-2")
                     .description("description-2")
                     .applyUntil(OffsetDateTime.now(ZoneOffset.UTC).plusDays(1))
                     .validUntil(OffsetDateTime.now(ZoneOffset.UTC).plusDays(2))
                     .build();

        when(repository.findById(2L)).thenReturn(Optional.of(job));

        Job returnedJob = jobsService.getJob(2);

        assertThat(returnedJob).isEqualTo(job);
    }

    @Test
    void throwsExceptionWhenJobNotFound() {
        when(repository.findById(10L)).thenThrow(new JobNotFoundException());

        assertThrows(JobNotFoundException.class, () -> jobsService.getJob(10));
    }

    @Test
    void createsNewJob() {
        CreateJobRequest createJobRequest = CreateJobRequest.builder()
                                                            .employerId(1)
                                                            .title("title-4")
                                                            .description("description-4")
                                                            .applyUntil(OffsetDateTime.now(ZoneOffset.UTC).plusDays(1))
                                                            .validUntil(OffsetDateTime.now(ZoneOffset.UTC).plusDays(2))
                                                            .build();

        Job savedJob = Job.builder()
                          .id(4L)
                          .employerId(1)
                          .title("title-4")
                          .description("description-4")
                          .applyUntil(OffsetDateTime.now(ZoneOffset.UTC).plusDays(1))
                          .validUntil(OffsetDateTime.now(ZoneOffset.UTC).plusDays(2))
                          .build();

        when(repository.save(createJobRequest.toJob())).thenReturn(savedJob);

        Job job = jobsService.createJob(createJobRequest);

        assertThat(job).isEqualTo(savedJob);
        verify(repository).save(createJobRequest.toJob());
    }

    @Test
    void updatesJobWithId() {
        OffsetDateTime validUntilUpdate = OffsetDateTime.now(ZoneOffset.UTC).plusDays(5);
        OffsetDateTime applyUntilUpdate = OffsetDateTime.now(ZoneOffset.UTC).plusDays(2);

        UpdateJobRequest jobUpdateData = new UpdateJobRequest("title-4", "description-4",
                validUntilUpdate, applyUntilUpdate, Job.JobStatus.DRAFT);

        Job job = Job.builder()
                     .id(2L)
                     .employerId(1)
                     .title("title-2")
                     .description("description-2")
                     .applyUntil(OffsetDateTime.now(ZoneOffset.UTC).plusDays(1))
                     .validUntil(OffsetDateTime.now(ZoneOffset.UTC).plusDays(2))
                     .status(Job.JobStatus.DRAFT)
                     .build();

        Job updatedJob = Job.builder()
                            .id(2L)
                            .employerId(1)
                            .title("title-4")
                            .description("description-4")
                            .applyUntil(applyUntilUpdate)
                            .validUntil(validUntilUpdate)
                            .status(Job.JobStatus.DRAFT)
                            .build();

        when(repository.findById(2L)).thenReturn(Optional.of(job));
        when(repository.save(updatedJob)).thenReturn(updatedJob);

        jobsService.updateJob(2, jobUpdateData);

        verify(repository).findById(2L);
        verify(repository).save(updatedJob);
    }

    @Test
    void deletesJobById() {
        jobsService.deleteJob(2);

        verify(repository).deleteById(2L);
    }

    @Test
    void searchesForJob() {
        Job job1 = Job.builder()
                      .id(3L)
                      .employerId(2)
                      .title("title-3")
                      .description("description-3")
                      .applyUntil(OffsetDateTime.now(ZoneOffset.UTC).plusDays(1))
                      .validUntil(OffsetDateTime.now(ZoneOffset.UTC).plusDays(2))
                      .build();

        Job job2 = Job.builder()
                      .id(4L)
                      .employerId(2)
                      .title("title-4")
                      .description("description-4")
                      .applyUntil(OffsetDateTime.now(ZoneOffset.UTC).plusDays(1))
                      .validUntil(OffsetDateTime.now(ZoneOffset.UTC).plusDays(2))
                      .build();

        List<Job> jobsFromRepository = List.of(job1, job2);

        when(repository.getAllByEmployerId(2)).thenReturn(jobsFromRepository);

        Iterable<Job> foundJobs = jobsService.search(2);

        assertThat(foundJobs).containsExactlyInAnyOrderElementsOf(jobsFromRepository);

        verify(repository).getAllByEmployerId(2);
    }

    @Test
    void returnsAllJobsIfNoSearchParametersArePassed() {
        Job job1 = Job.builder()
                      .id(2L)
                      .employerId(1)
                      .title("title-2")
                      .description("description-2")
                      .applyUntil(OffsetDateTime.now(ZoneOffset.UTC).plusDays(1))
                      .validUntil(OffsetDateTime.now(ZoneOffset.UTC).plusDays(2))
                      .build();

        Job job2 = Job.builder()
                      .id(3L)
                      .employerId(2)
                      .title("title-3")
                      .description("description-3")
                      .applyUntil(OffsetDateTime.now(ZoneOffset.UTC).plusDays(1))
                      .validUntil(OffsetDateTime.now(ZoneOffset.UTC).plusDays(2))
                      .build();

        Job job3 = Job.builder()
                      .id(4L)
                      .employerId(2)
                      .title("title-4")
                      .description("description-4")
                      .applyUntil(OffsetDateTime.now(ZoneOffset.UTC).plusDays(1))
                      .validUntil(OffsetDateTime.now(ZoneOffset.UTC).plusDays(2))
                      .build();

        List<Job> jobsFromRepository = List.of(job1, job2, job3);

        when(repository.findAll()).thenReturn(jobsFromRepository);

        jobsService.search(null);

        verify(repository).findAll();
    }
}
