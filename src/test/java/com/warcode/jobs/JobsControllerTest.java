package com.warcode.jobs;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.warcode.jobs.dto.CreateJobRequest;
import com.warcode.jobs.dto.UpdateJobRequest;
import com.warcode.jobs.enitity.Job;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.List;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(JobsController.class)
class JobsControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper mapper;

    @MockBean
    private JobsService service;

    @Test
    void listsJobs() throws Exception {
        Job job1 = Job.builder()
                      .id(1L)
                      .employerId(1)
                      .title("title-1")
                      .description("description-1")
                      .applyUntil(OffsetDateTime.now(ZoneOffset.UTC).plusDays(1))
                      .validUntil(OffsetDateTime.now(ZoneOffset.UTC).plusDays(2))
                      .build();

        Job job2 = Job.builder()
                      .id(2L)
                      .employerId(2)
                      .title("title-2")
                      .description("description-2")
                      .applyUntil(OffsetDateTime.now(ZoneOffset.UTC).plusDays(1))
                      .validUntil(OffsetDateTime.now(ZoneOffset.UTC).plusDays(2))
                      .build();

        List<Job> jobsList = List.of(job1, job2);

        when(service.listJobs()).thenReturn(jobsList);

        mockMvc.perform(get("/jobs").contentType(MediaType.APPLICATION_JSON))
               .andExpect(status().isOk())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON))
               .andExpect(content().json(mapper.writeValueAsString(jobsList)));
    }

    @Test
    void getsJobById() throws Exception {
        Job job = Job.builder()
                     .id(1L)
                     .employerId(1)
                     .title("title-1")
                     .description("description-1")
                     .applyUntil(OffsetDateTime.now(ZoneOffset.UTC).plusDays(1))
                     .validUntil(OffsetDateTime.now(ZoneOffset.UTC).plusDays(2))
                     .build();

        when(service.getJob(1)).thenReturn(job);

        mockMvc.perform(get("/jobs/{jobId}", 1).contentType(MediaType.APPLICATION_JSON))
               .andExpect(status().isOk())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON))
               .andExpect(content().json(mapper.writeValueAsString(job)));
    }

    @Test
    void createsNewJob() throws Exception {
        OffsetDateTime validUntil = OffsetDateTime.now(ZoneOffset.UTC).plusDays(2);
        OffsetDateTime applyUntil = OffsetDateTime.now(ZoneOffset.UTC).plusDays(1);

        CreateJobRequest createJobRequest = CreateJobRequest.builder()
                                                            .employerId(1)
                                                            .title("title-1")
                                                            .description("description-1")
                                                            .applyUntil(applyUntil)
                                                            .validUntil(validUntil)
                                                            .status(Job.JobStatus.DRAFT)
                                                            .build();

        Job createdJob = Job.builder()
                            .id(1L)
                            .employerId(1)
                            .title("title-1")
                            .description("description-1")
                            .applyUntil(applyUntil)
                            .validUntil(validUntil)
                            .createdOn(OffsetDateTime.now(ZoneOffset.UTC).minusDays(1))
                            .updatedOn(OffsetDateTime.now(ZoneOffset.UTC).minusHours(1))
                            .status(Job.JobStatus.DRAFT)
                            .build();

        when(service.createJob(createJobRequest)).thenReturn(createdJob);

        mockMvc.perform(post("/jobs").contentType(MediaType.APPLICATION_JSON)
                                     .content(mapper.writeValueAsString(createJobRequest)))
               .andExpect(status().isCreated())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON))
               .andExpect(content().json(mapper.writeValueAsString(createdJob)));
    }

    @Test
    void updatesJobWithId() throws Exception {
        UpdateJobRequest job = new UpdateJobRequest("title-1", "description-1",
                OffsetDateTime.now(ZoneOffset.UTC).plusDays(1), OffsetDateTime.now(ZoneOffset.UTC).plusDays(2),
                Job.JobStatus.DRAFT);

        mockMvc.perform(put("/jobs/{jobId}", 1L).contentType(MediaType.APPLICATION_JSON)
                                                .content(mapper.writeValueAsString(job)))
               .andExpect(status().isNoContent());
    }

    @Test
    void deletesJobById() throws Exception {
        mockMvc.perform(delete("/jobs/{jobId}", 1).contentType(MediaType.APPLICATION_JSON))
               .andExpect(status().isNoContent());
    }

    @Test
    void searchesForJobs() throws Exception {
        Job job1 = Job.builder()
                      .id(2L)
                      .employerId(3)
                      .title("title-1")
                      .description("description-1")
                      .applyUntil(OffsetDateTime.now(ZoneOffset.UTC).plusDays(1))
                      .validUntil(OffsetDateTime.now(ZoneOffset.UTC).plusDays(2))
                      .build();

        Job job2 = Job.builder()
                      .id(2L)
                      .employerId(2)
                      .title("title-2")
                      .description("description-2")
                      .applyUntil(OffsetDateTime.now(ZoneOffset.UTC).plusDays(1))
                      .validUntil(OffsetDateTime.now(ZoneOffset.UTC).plusDays(2))
                      .build();

        List<Job> jobsList = List.of(job1, job2);

        when(service.search(2)).thenReturn(jobsList);

        mockMvc.perform(get("/jobs/search").contentType(MediaType.APPLICATION_JSON)
                                           .param("employerId", "2"))
               .andExpect(status().isOk())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON))
               .andExpect(content().json(mapper.writeValueAsString(jobsList)));
    }

}
