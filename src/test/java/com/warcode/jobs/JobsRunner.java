package com.warcode.jobs;

import com.intuit.karate.junit5.Karate;

public class JobsRunner {

    @Karate.Test
    Karate jobsTests() {
        return Karate.run("jobs").relativeTo(getClass());
    }
}
