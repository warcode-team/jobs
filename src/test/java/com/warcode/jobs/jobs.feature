Feature: Jobs

  Scenario: List jobs
    Given url urlBase + '/jobs'

    When method get

    Then status 200
    And header Content-Type = application/json
    And match response[0] contains { "id": "#notnull", "employerId": 1, "title": "Java web app" }
    And match response[0] contains { "description": "Simple web application", "status": "DRAFT" }
    And match response[0] contains { "validUntil": "2019-10-20T02:00:00+02:00", "applyUntil": "2019-10-15T02:00:00+02:00" }
    And match response[0] contains { "createdOn": "2020-02-20T00:51:44.861106+01:00", "updatedOn": "2020-02-20T00:51:44.861725+01:00" }