package com.warcode.jobs;

import com.warcode.jobs.enitity.Job;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;

import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.time.temporal.ChronoUnit;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.within;

@DataJpaTest
class JobsRepositoryTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private JobsRepository repository;

    @Test
    void listsJobsCorrectly() {
        Job job1 = Job.builder()
                      .employerId(1)
                      .title("title-1")
                      .description("description-1")
                      .applyUntil(OffsetDateTime.now(ZoneOffset.UTC).plusDays(1))
                      .validUntil(OffsetDateTime.now(ZoneOffset.UTC).plusDays(2))
                      .status(Job.JobStatus.DRAFT)
                      .build();

        Job job2 = Job.builder()
                      .employerId(2)
                      .title("title-2")
                      .description("description-2")
                      .applyUntil(OffsetDateTime.now(ZoneOffset.UTC).plusDays(1))
                      .validUntil(OffsetDateTime.now(ZoneOffset.UTC).plusDays(2))
                      .status(Job.JobStatus.DRAFT)
                      .build();

        Job databaseJob1 = entityManager.persist(job1);
        Job databaseJob2 = entityManager.persist(job2);

        Iterable<Job> jobs = repository.findAll();

        assertThat(jobs).containsExactlyInAnyOrder(databaseJob1, databaseJob2);
    }

    @Test
    void fetchesJobCorrectly() {
        Job job = Job.builder()
                     .employerId(1)
                     .title("title-1")
                     .description("description-1")
                     .applyUntil(OffsetDateTime.now(ZoneOffset.UTC).plusDays(1))
                     .validUntil(OffsetDateTime.now(ZoneOffset.UTC).plusDays(2))
                     .status(Job.JobStatus.DRAFT)
                     .build();

        Job databaseJob = entityManager.persist(job);

        Optional<Job> foundJob = repository.findById(databaseJob.getId());

        assertThat(foundJob).isNotEmpty();
        assertThat(foundJob.get()).isEqualToIgnoringGivenFields(job, "id", "created_on", "updated_on");
        assertThat(foundJob.get().getId()).isPositive();
        assertThat(foundJob.get()
                           .getCreatedOn()).isCloseTo(OffsetDateTime.now(ZoneOffset.UTC), within(10, ChronoUnit.SECONDS));
        assertThat(foundJob.get()
                           .getUpdatedOn()).isCloseTo(OffsetDateTime.now(ZoneOffset.UTC), within(10, ChronoUnit.SECONDS));
    }

    @Test
    void jobIsPersistedCorrectly() {
        Job job = Job.builder()
                     .employerId(1)
                     .title("title-1")
                     .description("description-1")
                     .applyUntil(OffsetDateTime.now(ZoneOffset.UTC).plusDays(1))
                     .validUntil(OffsetDateTime.now(ZoneOffset.UTC).plusDays(2))
                     .status(Job.JobStatus.DRAFT)
                     .build();

        Job savedJob = repository.save(job);

        assertThat(savedJob).isEqualToIgnoringGivenFields(job, "id", "created_on", "updated_on");
        assertThat(savedJob.getId()).isPositive();
        assertThat(savedJob.getCreatedOn()).isCloseTo(OffsetDateTime.now(ZoneOffset.UTC), within(10, ChronoUnit.SECONDS));
        assertThat(savedJob.getUpdatedOn()).isCloseTo(OffsetDateTime.now(ZoneOffset.UTC), within(10, ChronoUnit.SECONDS));
    }

    @Test
    void jobIsUpdatedCorrectly() {
        Job job = Job.builder()
                     .employerId(1)
                     .title("title-1")
                     .description("description-1")
                     .applyUntil(OffsetDateTime.now(ZoneOffset.UTC).plusDays(1))
                     .validUntil(OffsetDateTime.now(ZoneOffset.UTC).plusDays(2))
                     .status(Job.JobStatus.DRAFT)
                     .build();

        Job databaseJob = entityManager.persistFlushFind(job);
        databaseJob.setEmployerId(1);
        databaseJob.setTitle("title-2");
        databaseJob.setDescription("description-2");
        databaseJob.setApplyUntil(OffsetDateTime.now(ZoneOffset.UTC).plusDays(2));
        databaseJob.setValidUntil(OffsetDateTime.now(ZoneOffset.UTC).plusDays(5));

        Job savedJob = repository.save(databaseJob);

        assertThat(savedJob).isEqualToIgnoringGivenFields(databaseJob, "id", "created_on", "updated_on");
        assertThat(savedJob.getId()).isEqualTo(databaseJob.getId());
        assertThat(savedJob.getCreatedOn()).isEqualTo(job.getCreatedOn());
        assertThat(savedJob.getUpdatedOn()).isCloseTo(OffsetDateTime.now(ZoneOffset.UTC), within(10, ChronoUnit.SECONDS));
    }

    @Test
    void jobIsDeletedCorrectly() {
        Job job = Job.builder()
                     .employerId(1)
                     .title("title-1")
                     .description("description-1")
                     .applyUntil(OffsetDateTime.now(ZoneOffset.UTC).plusDays(1))
                     .validUntil(OffsetDateTime.now(ZoneOffset.UTC).plusDays(2))
                     .status(Job.JobStatus.DRAFT)
                     .build();

        Job databaseJob = entityManager.persist(job);

        repository.deleteById(databaseJob.getId());

        assertThat(repository.findById(databaseJob.getId())).isEmpty();
    }

    @Test
    void findsJobsByEmployerId() {
        Job job1 = Job.builder()
                      .employerId(1)
                      .title("title-1")
                      .description("description-1")
                      .applyUntil(OffsetDateTime.now(ZoneOffset.UTC).plusDays(1))
                      .validUntil(OffsetDateTime.now(ZoneOffset.UTC).plusDays(2))
                      .status(Job.JobStatus.DRAFT)
                      .build();

        Job job2 = Job.builder()
                      .employerId(2)
                      .title("title-2")
                      .description("description-2")
                      .applyUntil(OffsetDateTime.now(ZoneOffset.UTC).plusDays(1))
                      .validUntil(OffsetDateTime.now(ZoneOffset.UTC).plusDays(2))
                      .status(Job.JobStatus.DRAFT)
                      .build();

        entityManager.persist(job1);
        entityManager.persist(job2);

        Iterable<Job> jobs = repository.getAllByEmployerId(2);

        assertThat(jobs).containsExactly(job2);
    }
}
