package com.warcode.jobs;

import com.warcode.jobs.enitity.Job;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.json.JsonTest;
import org.springframework.boot.test.json.JacksonTester;

import java.time.OffsetDateTime;

import static org.assertj.core.api.Assertions.assertThat;

@JsonTest
class JobTest {

    // Bug: https://youtrack.jetbrains.com/issue/IDEA-168250
    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
    @Autowired
    private JacksonTester<Job> jacksonTester;

    @Test
    void testSerialize() throws Exception {
        Job job = Job.builder()
                     .id(1L)
                     .employerId(1)
                     .title("title-1")
                     .description("description-1")
                     .applyUntil(OffsetDateTime.parse("2019-10-14T00:02:15.864Z"))
                     .validUntil(OffsetDateTime.parse("2019-10-16T00:02:15.864Z"))
                     .createdOn(OffsetDateTime.parse("2019-10-12T00:02:15.864Z"))
                     .updatedOn(OffsetDateTime.parse("2019-10-12T00:02:15.864Z"))
                     .status(Job.JobStatus.DRAFT)
                     .build();

        assertThat(jacksonTester.write(job)).isEqualToJson(
                """
                {
                    "id": 1,
                    "employerId": 1,
                    "title": "title-1",
                    "description": "description-1",
                    "status": "DRAFT",
                    "applyUntil": "2019-10-14T00:02:15.864Z",
                    "validUntil": "2019-10-16T00:02:15.864Z",
                    "createdOn": "2019-10-12T00:02:15.864Z",
                    "updatedOn": "2019-10-12T00:02:15.864Z"
                }
                """);
    }

    @Test
    void testDeserialize() throws Exception {
        String content = """
                {
                    "id": 1,
                    "employerId": 1,
                    "title": "title-1",
                    "description": "description-1",
                    "status": "DRAFT",
                    "validUntil": "2019-10-14T00:02:15.864Z",
                    "applyUntil": "2019-10-16T00:02:15.864Z",
                    "createdOn": "2019-10-12T00:02:15.864Z",
                    "updatedOn": "2019-10-12T00:02:15.864Z"
                }
                """;

        Job job = Job.builder()
                     .id(1L)
                     .employerId(1)
                     .title("title-1")
                     .description("description-1")
                     .validUntil(OffsetDateTime.parse("2019-10-14T00:02:15.864Z"))
                     .applyUntil(OffsetDateTime.parse("2019-10-16T00:02:15.864Z"))
                     .createdOn(OffsetDateTime.parse("2019-10-12T00:02:15.864Z"))
                     .updatedOn(OffsetDateTime.parse("2019-10-12T00:02:15.864Z"))
                     .status(Job.JobStatus.DRAFT)
                     .build();

        assertThat(jacksonTester.parse(content)).isEqualTo(job);
    }
}
